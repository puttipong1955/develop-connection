const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')
const app = express()

const Posts = require('./routes/api/posts')
const Profiles = require('./routes/api/profiles')
const Users = require('./routes/api/users')

// Body-parser middleware
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// Connect DB
const db = require('./config/keys').mongoURI
mongoose.connect(db)
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err))

app.get('/', (req, res) => res.send('Hello'))

// Passport Middleware
app.use(passport.initialize())

// Passport config
require('./config/passport')(passport)

// Use Routes
app.use('/api/users', Users)
app.use('/api/profiles', Profiles)
app.use('/api/posts', Posts)

const port = process.env.POST || 5000

app.listen(port, () => console.log(`Server running on port ${port}`))