import React from 'react'
import Spinner from './spinner.gif'

export default () => {
  return (
    <div>
        <img
            src={Spinner}
            style={{width: '220px', maigin: 'auto', display: 'block'}}
            alt="loadlong..."
        />
    </div>
  )
}
